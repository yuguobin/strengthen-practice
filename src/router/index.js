import father from "../views/father.vue"
import { createRouter,createWebHistory } from "vue-router"

const routes = [{
    path:'/',
    component:father
}]
const router =createRouter({
    history:createWebHistory(),
    routes
})
export default router;